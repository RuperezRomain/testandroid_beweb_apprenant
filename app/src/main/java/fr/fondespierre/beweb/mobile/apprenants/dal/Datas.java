package fr.fondespierre.beweb.mobile.apprenants.dal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.fondespierre.beweb.mobile.apprenants.dal.enumerations.Status;

/**
 * Created by ruperez on 05/07/17.
 */

public class Datas {

    public static JSONObject apprenant;


    public static JSONArray getApprenants() throws JSONException {
        JSONArray apprenants = new JSONArray();
        JSONObject alex = new JSONObject();
        JSONObject jc = new JSONObject();
        JSONObject marie = new JSONObject();

        alex.putOpt("id",1);
        alex.putOpt("nom","LeForestier");
        alex.putOpt("prenom","Alexandre");
        alex.putOpt("age",30);
        alex.putOpt("promotion",1);
        alex.putOpt("session",1);
        alex.putOpt("status", Status.Chomage);

        jc.putOpt("id",2);
        jc.putOpt("nom","Petetin");
        jc.putOpt("prenom","Jean-Christian");
        jc.putOpt("age",25);
        jc.putOpt("promotion",2);
        jc.putOpt("session",2);
        jc.putOpt("status",Status.Formation);

        marie.putOpt("id",3);
        marie.putOpt("nom","Urbano");
        marie.putOpt("prenom","Marie");
        marie.putOpt("age",25);
        marie.putOpt("promotion",2);
        marie.putOpt("session",2);
        marie.putOpt("status",Status.Formation);

        apprenants.put(alex);
        apprenants.put(jc);
        apprenants.put(marie);

        return apprenants;
    }

public static JSONArray getPromotions() throws JSONException {
    JSONArray promotions = new JSONArray();
    JSONObject lunel1 = new JSONObject();
    JSONObject lunel2 = new JSONObject();
    JSONObject beziers1 = new JSONObject();
    lunel1.putOpt("id", 1);
    lunel1.putOpt("session", 1);
    lunel1.putOpt("ville", "lunel");
    lunel1.putOpt("debut", "2016");
    lunel1.putOpt("fin", "2017");
    lunel2.putOpt("id", 2);
    lunel2.putOpt("session", 2);
    lunel2.putOpt("ville", "lunel");
    lunel2.putOpt("debut", "2015");
    lunel2.putOpt("fin", "2016");
    beziers1.putOpt("id", 3);
    beziers1.putOpt("session", 2);
    beziers1.putOpt("ville", "béziers");
    beziers1.putOpt("debut", "2016");
    beziers1.putOpt("fin", "2017");
    promotions.put(lunel1);
    promotions.put(lunel2);
    promotions.put(beziers1);
    return promotions;
}

    /**
     * Minimum 4 projets par apprenants
     *
     * cette méthode crée tous les projets de tous les apprenants
     * @return
     */
    private static JSONArray getProjets() throws JSONException {

        JSONArray Projets = new JSONArray();
        JSONObject BeWeb = new JSONObject();
        JSONObject FindMyCoop = new JSONObject();
        JSONObject AdoptUnBoss = new JSONObject();
        JSONObject Pizzaioli = new JSONObject();

        BeWeb.putOpt("id",1);
        BeWeb.putOpt("url_github","http://github.com");
        BeWeb.putOpt("url_site","http://google.com");
        BeWeb.putOpt("nom","BeWeb");
        BeWeb.putOpt("Apprenant",1);

        FindMyCoop.putOpt("id",2);
        FindMyCoop.putOpt("url_github","http://github.com");
        FindMyCoop.putOpt("url_site","http://google.com");
        FindMyCoop.putOpt("nom","FindMyCoop");
        FindMyCoop.putOpt("Apprenant",1);

        AdoptUnBoss.putOpt("id",3);
        AdoptUnBoss.putOpt("url_github","http://github.com");
        AdoptUnBoss.putOpt("url_site","http://google.com");
        AdoptUnBoss.putOpt("nom","AdoptUnBoss");
        AdoptUnBoss.putOpt("Apprenant",2);

        Pizzaioli.putOpt("id",4);
        Pizzaioli.putOpt("url_github","http://github.com");
        Pizzaioli.putOpt("url_site","http://google.com");
        Pizzaioli.putOpt("nom","Pizzaioli");
        Pizzaioli.putOpt("Apprenant",2);

        Projets.put(BeWeb);
        Projets.put(FindMyCoop);
        Projets.put(AdoptUnBoss);
        Projets.put(Pizzaioli);

        return Projets;

    }

    /**
     * Appelle la méthode getProjet() afin de récuprer la liste de rous les projets
     * @param apprenantID
     * @return
     */
    private static JSONArray getProjets(int apprenantID) throws JSONException {

        JSONArray projets = Datas.getProjets() ;
        JSONArray projetApprenants = new JSONArray();

        for(int i = 0 ; i < projets.length(); i ++) {
            if(projets.getJSONObject(i).getInt("Apprenant") == apprenantID){
                projetApprenants.put(projets.getJSONObject(i));
            }
        }

        return projetApprenants;

    }


    /**
     * Minimum 6 skills par apprenants
     * @return
     */
    private static JSONArray getSkills() throws JSONException {
        JSONArray Skills = new JSONArray();
        JSONObject Html = new JSONObject();
        JSONObject Css = new JSONObject();
        JSONObject Php = new JSONObject();
        JSONObject JavaScript = new JSONObject();
        JSONObject Ruby = new JSONObject();
        JSONObject Python = new JSONObject();

        Html.putOpt("id",1);
        Html.putOpt("nom","Html");
        Html.putOpt("value","");
        Html.putOpt("apprenant",1);

        Css.putOpt("id",2);
        Css.putOpt("nom","Css");
        Css.putOpt("value","");
        Css.putOpt("apprenant",1);

        Php.putOpt("id",3);
        Php.putOpt("nom","Php");
        Php.putOpt("value","");
        Php.putOpt("apprenant",1);


        JavaScript.putOpt("id",4);
        JavaScript.putOpt("nom","JavaScript");
        JavaScript.putOpt("value","");
        JavaScript.putOpt("apprenant",1);


        Ruby.putOpt("id",5);
        Ruby.putOpt("nom","Ruby");
        Ruby.putOpt("value","");
        Ruby.putOpt("apprenant",2);


        Python.putOpt("id",6);
        Python.putOpt("nom","Python");
        Python.putOpt("value","");
        Python.putOpt("apprenant",2);


        Skills.put(Html);
        Skills.put(Css);
        Skills.put(Php);
        Skills.put(JavaScript);
        Skills.put(Ruby);
        Skills.put(Python);

        return Skills;
    }

    /**
     *
     * @param apprenantID
     * @return
     */
    private static JSONArray getSkills(int apprenantID) throws JSONException {


        JSONArray skills = Datas.getSkills() ;
        JSONArray skillsApprenant = new JSONArray();

        for(int i = 0 ; i < skills.length(); i ++) {
            if(skills.getJSONObject(i).getInt("apprenant") == apprenantID){
                skillsApprenant.put(skills.getJSONObject(i));
            }
        }

        return skillsApprenant;

    }

    /**
     * L'apprenant a des skills et des projets
     * @param id
     * @return
     * @throws JSONException
     */
    public static JSONObject getApprenant(int id) throws JSONException {

        //recupere la liste des apprenants
        JSONArray apprenants = Datas.getApprenants() ;
        JSONObject apprenant = new JSONObject();

        //on boucle sur la longueur de la liste d'apprenant
        for(int i = 0 ; i < apprenants.length(); i++) {
            //si l'id match avec l'id en parametre (sur l'index courant)
            if(apprenants.getJSONObject(i).getInt("id") == id){
                //affectation de l'apprenant objet selectionné
                apprenant = apprenants.getJSONObject(i);
//          liste des skills par id dans la var skills
            JSONArray skills = Datas.getSkills(id) ;
                //pareil pour les projets
            JSONArray projets = Datas.getProjets(id) ;
                //on ajoute la propriété skill à l'apprenant
            apprenant.putOpt("skill",skills);
                //pareil pour les projets
            apprenant.putOpt("projet",projets);
            }
        }


        return apprenant;

    }

}
