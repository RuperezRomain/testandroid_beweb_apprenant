package fr.fondespierre.beweb.mobile.apprenants;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import fr.fondespierre.beweb.mobile.apprenants.adapters.ProjetAdapter;
import fr.fondespierre.beweb.mobile.apprenants.dal.Datas;

import static fr.fondespierre.beweb.mobile.apprenants.dal.Datas.apprenant;

public class DetailApprenantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_apprenant);


        try {
//            JSONObject apprenant = apprenant;
            ((TextView)findViewById(R.id.da_textView_nom)).setText(apprenant.getString("nom"));
            ((TextView)findViewById(R.id.da_textView_prenom)).setText(apprenant.getString("prenom"));
            ((TextView)findViewById(R.id.da_textView_age)).setText(apprenant.getString("age"));
            ((TextView)findViewById(R.id.da_textView_email)).setText(apprenant.getString("email"));
//            ((TextView)findViewById(R.id.da_textView_tel)).setText(apprenant.getString("tel"));
            ((TextView)findViewById(R.id.da_textView_status)).setText(apprenant.getString("status"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ListView listeProjets = (ListView) findViewById(R.id.da_listView_projetsd);


        ProjetAdapter projetAdapter = null;


        try {

            projetAdapter = new ProjetAdapter(this,0,apprenant);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        listeProjets.setAdapter(projetAdapter);





    }
}
