package fr.fondespierre.beweb.mobile.apprenants.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.SpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import fr.fondespierre.beweb.mobile.apprenants.DetailApprenantActivity;
import fr.fondespierre.beweb.mobile.apprenants.ListeApprenantsActivity;
import fr.fondespierre.beweb.mobile.apprenants.R;
import fr.fondespierre.beweb.mobile.apprenants.dal.Datas;

/**
 * Created by ruperez on 05/07/17.
 */

public class ListeApprenantAdapter extends ArrayAdapter {


    private final Activity activity;
    private final int resource = R.layout.liste_apprenant_item;
    private final JSONArray apprenants;
//    private final JSONArray promotions;

    public ListeApprenantAdapter(@NonNull Activity activity, @LayoutRes int resource, JSONArray liste) {
        super(activity.getApplicationContext(), resource);
        this.activity = activity;
        this.apprenants = liste;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final int index = position;
        LayoutInflater inflater = activity.getLayoutInflater();
        convertView = inflater.inflate(resource,null);
        TextView textNom = (TextView)convertView.findViewById(R.id.laItem_textView_nom);
        TextView textPrenom = (TextView)convertView.findViewById(R.id.laItem_textView_prenom);
        Spinner textPromo = (Spinner)convertView.findViewById(R.id.la_spinner_promo);
        ImageButton detail = (ImageButton)convertView.findViewById(R.id.la_listView_apprenants);


        detail.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v){
                                          try {
                                              Intent intent = new Intent(activity.getApplicationContext(), DetailApprenantActivity.class);

                                              Datas.apprenant = apprenants.getJSONObject(index);
                                              activity.startActivity(intent);
                                          } catch (JSONException e) {
                                              e.printStackTrace();
                                          }

                                      }
                                  }
        );


        try {
            textNom.setText(apprenants.getJSONObject(position).getString("nom"));
            textPrenom.setText(apprenants.getJSONObject(position).getString("prenom"));
//            textPromo.setAdapter(SpinnerAdapter,promotions,R.layout.simple_spinner_item);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return apprenants.length();
    }


}
