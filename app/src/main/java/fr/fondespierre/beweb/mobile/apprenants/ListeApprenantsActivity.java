package fr.fondespierre.beweb.mobile.apprenants;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import fr.fondespierre.beweb.mobile.apprenants.adapters.ListeApprenantAdapter;
import fr.fondespierre.beweb.mobile.apprenants.dal.Datas;

public class ListeApprenantsActivity extends AppCompatActivity {

    //this code will be executed
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //executed code at the launch activity
        super.onCreate(savedInstanceState);
        //set the layout view work on
        setContentView(R.layout.liste_apprenants);
        //select an object ListView by id
        final ListView listeApprenant = (ListView) findViewById(R.id.la_listView_apprenants);
        //initialize JSONArray listeDatas who will contain the result query
        final JSONArray listeDatas = null;
        //declare the current activity
        final Activity activity = this;

        //declare http request with parameter activity
        RequestQueue request = Volley.newRequestQueue(getApplicationContext());
        //declare url variable of API
        String url = "http://192.168.1.48/beweb_api/index.php/";
        //GET request with uri "apprenants"
        //stock in listeDatas
        //initialise the response.listener in JSONArray
        JsonArrayRequest jr = new JsonArrayRequest(Request.Method.GET, url + "apprenants", listeDatas, new Response.Listener<JSONArray>() {
            @Override
            //at the success response
            public void onResponse(JSONArray response) {

                //call the Adapter and create one in the current activity
                //select the layout with the list response
                ListeApprenantAdapter adapter = new ListeApprenantAdapter(activity,R.layout.liste_apprenant_item,response);
                //provide the data and the view
                listeApprenant.setAdapter(adapter);


            }
        },
                //at the error of response
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        //execture request jr for GET list of apprenants
        request.add(jr);


        /**
         * Test de récupération des villes avec l'API
         * Pas encore fonctionnel
         */
//        final JSONArray listePromos = null;
//
//        final Spinner SpinnerPromo = (Spinner) findViewById(R.id.la_spinner_promo);
//
//        JsonArrayRequest promoRequest = new JsonArrayRequest(Request.Method.GET, url + "villes", listePromos, new Response.Listener<JSONArray>() {
//            @Override
//            //at the success response
//            public void onResponse(JSONArray response) {
//
//                ArrayAdapter adapterSpinner = new ArrayAdapter(activity,android.R.layout.simple_spinner_item, (List) listePromos) {
//                };
//                SpinnerPromo.setAdapter(adapterSpinner);
//
//
//            }
//        },
//                //at the error of response
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                });
//
//
//        request.add(promoRequest);
//
//
////        ArrayList<String> liste = new ArrayList<>();
//
//        Spinner spinner = (Spinner) findViewById(R.id.la_spinner_promo);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter adapt = new ArrayAdapter(activity,android.R.layout.simple_spinner_item,(List)listePromos);
//
//// Apply the adapter to the spinner
//        spinner.setAdapter(adapt);


    }
}
